window.onload = fetchRiddles;

async function fetchRiddles() {
    fetch(`http://0.0.0.0:8000/riddles`)
        .then(response => response.json())
        .then(json => {
            for (let i = 0; i < json.length; i++) {
                var ul = document.getElementById('riddles')
                var li = document.createElement('li')
                li.appendChild(document.createTextNode(`${json[i].riddle}, ${json[i].option}`))
                ul.appendChild(li)
            }
        })
}
